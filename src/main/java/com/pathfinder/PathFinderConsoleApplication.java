package com.pathfinder;

import com.pathfinder.UpdateData.stationUpdater;
import com.pathfinder.model.*;
import com.pathfinder.search.SimpleSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import com.pathfinder.model.DAO.*;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
@EnableAutoConfiguration
@EnableSpringConfigured
@ComponentScan
public class PathFinderConsoleApplication {

	public static void main(String[] args) {


		final ConfigurableApplicationContext context = SpringApplication.run(PathFinderConsoleApplication.class, args);
		final PathFinderConsoleApplication app = context.getBean(PathFinderConsoleApplication.class);
		app.run(args);
	}

	private void run(String[] args) {
//		Scanner s = new Scanner(System.in);
//
//		//stationUpdater updater = new stationUpdater();
//
		//updater.updateStationsConnetion();
		try {
			updater.updateStationsConnetion();
			updater.makeSimpleStationConnections();
			updater.getAllConnections();
		}
		catch (Exception ex){
			System.out.println(ex.getMessage());
		}


	}


	@Autowired
	private DbConnectionDAO connectionDAO;

	public DbConnectedStationDAO getConnectedStationDAO() {
		return connectedStationDAO;
	}

	@Autowired
	private DbConnectedStationDAO connectedStationDAO;

	public DbConnectionDAO getConnectionDAO() {
		return connectionDAO;
	}

	public DbSimpleStationDAO getSimpleStationDAO() {
		return simpleStationDAO;
	}

	@Autowired
	private DbSimpleStationDAO simpleStationDAO;

	@Resource(name="stationUpdater")
	private stationUpdater updater;


}
