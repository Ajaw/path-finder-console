package com.pathfinder.search;


import  com.pathfinder.model.*;
import  com.pathfinder.RailwayModel.structure.*;
import com.pathfinder.model.DAO.DbSimpleStationDAO;
import com.pathfinder.model.DAO.DbStationDAO;
import com.pathfinder.model.precomputedData.PartStructure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 18.04.2017.
 */

public class ConnectionSearch {

    private List<dbStation> stationRepository;

    private int numberOfSwitches;

    private List<ParentStructure> queue;

    private List<dbConnectedStation> stationOnConnections;



    public List<List<dbConnectedStation>> getAllRoutes() {
        return allRoutes;
    }

    public List<List<dbConnectedStation>> allRoutes;

    private List<dbSimpleStation> route;

    private HashSet<Integer> alreadyAddedConnections;

    public List<PartStructure> getPartStructures() {
        return partStructures;
    }

    private List<PartStructure> partStructures;


    public ConnectionSearch(List<dbStation> rep) {

        partStructures = new LinkedList<>();
        stationRepository = rep;
    }



    public void findConnections(List<dbSimpleStation> route){

        dbSimpleStation start = route.get(0);

        dbStation stratStation = getStationByID(start.motherStationID);

        this.route = route;
        numberOfSwitches = 0;
        stationOnConnections = new LinkedList<>();
        queue = new LinkedList<>();
        alreadyAddedConnections = new HashSet<>();
        allRoutes = new LinkedList<>();
        for (int i = 0; i < 3; i++) {
            numberOfSwitches = i;
            try {
                loop(new LinkedList<>(stratStation.getDirectStationConnection()),
                        null, 0, 0, null, true);
            }
            catch (Exception ex){
                System.out.println();
            }
            if (queue.size()>0){
                retriveDataFromSearch();
                break;
            }
        }

        if(allRoutes.size()>0){
            PartStructure temp = new PartStructure();
            temp.setStationList(route);
            temp.setConnectionsList(allRoutes);
            partStructures.add(temp);
        }




      //  System.out.printf("test");

    }

    private void loop(List<dbConnectedStation> connectedStations, dbConnectedStation previousStation,
                      int current_level , int numer_of_switches, ParentStructure structure, boolean first_flag){
        //  List<dbStationOnConnection> stationOnConnections = SQLite.select().from(dbStationOnConnection.class).queryList();


        int number_of_iter = connectedStations.size();

        dbSimpleStation currentRouteStation;
        current_level++;
        if (current_level==route.size()){
            currentRouteStation=null;
        }
        else {
            currentRouteStation = route.get(current_level);
        }

        try {
            ParentStructure previousStructer = structure;
            int previous_number = numer_of_switches;
            for (int i = 0; i < number_of_iter; i++) {
                numer_of_switches = previous_number;

                    if (alreadyAddedConnections.contains(connectedStations.get(i).connectionID)) {
                        continue;
                    }
//                if (first_flag) {
//                    previousStructer = null;
//
//                    previousStation = null;
//                    List<dbConnectedStation> list1 = SearchHelper.getStationByID((connectedStations.get(i).connectedStationID),stationRepository).getDirectStationConnection();
//                    if (list1.size() > 0) {
//                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
//
//                        // queue.add(p);
//
//                        loop(list1, previousStation,current_level,numer_of_switches,
//                                 p, false);
//
//                    }
//                    connectedStations.get(i).visited = true;
//                }
//
//                else  {
                    if (previousStation != null) {
                        if (connectedStations.get(i).connectionID != previousStation.connectionID) {
                            numer_of_switches++;
                        }
                    }

                    if (numer_of_switches >= numberOfSwitches) {

                        connectedStations.get(i).visited = true;
                        continue;
                        // numer_of_switches--;

                    } else if (currentRouteStation == null) {
                        connectedStations.get(i).visited = true;
                        stationOnConnections.add(connectedStations.get(i));
                        alreadyAddedConnections.add(connectedStations.get(i).connectionID);
                        ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));
                        p.setFinalStation(true);
                        p.setStructure(previousStructer);
                        queue.add(p);

                        break;

                    } else if (connectedStations.get(i).getConnectedStationID() == currentRouteStation.getMotherStationID()) {
                        List<dbConnectedStation> list1 =
                               new LinkedList<>(getStationByID(connectedStations.get(i).connectedStationID).getDirectStationConnection());
                        if (list1.size() > 0) {
                            ParentStructure p = new ParentStructure(previousStation, connectedStations.get(i));

//                            if(first_flag) {
//                                p = new ParentStructure(null, connectedStations.get(i));
//                            }
                            p.setStructure(previousStructer);
                            //queue.add(p);
                            // previousStructer = p;
                            loop(list1, connectedStations.get(i), current_level, numer_of_switches
                                    , p, false);
                            connectedStations.get(i).visited = true;
                        }
                        connectedStations.get(i).visited = true;
                    }
                    connectedStations.get(i).visited = true;
                }


           // }
        }
        catch (Exception ex){

            System.out.printf("etst");
        }
    }

    private void retriveDataFromSearch(){
        try {


            for (ParentStructure structure : queue) {
                if (structure.isFinalStation()) {
                    retraceRoute(structure);
                }
            }
        }
        catch(Exception ex){
            System.out.println("blad");
            }


    }

    private void retraceRoute(ParentStructure p){
        List<dbConnectedStation> route = new ArrayList<>();
        do {
            route.add(p.getStation());

            p = p.getStructure();
        }  while (p.getStructure()!=null);
        route.add(p.getStation());
        allRoutes.add(reversList(route));


        System.out.println("retracing route");
    }

    private List<dbConnectedStation> reversList(List<dbConnectedStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }

    private dbStation getStationByID(int id){
        dbStation retStatio = new dbStation();
        for (dbStation station : stationRepository){
           // System.out.printf("test");
            if (station.getId() == id){
               // System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }
}

