package com.pathfinder.search;

import com.pathfinder.RailwayModel.structure.SimpleParentStructure;
import com.pathfinder.model.DAO.*;
import com.pathfinder.model.*;
import com.pathfinder.model.precomputedData.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

@Component("dfsSearch")
@Transactional
public class DfsSearch {



    private dbStation startStation;

    private dbStation finishStation;

    private List<dbStation> stationRepository;

    private List<dbSimpleStation> list;


    @Autowired
    private DbPrecomputedPartsDAO precomputedPartsDAO;

    @Autowired
    private DbPrecomputedConnectionsDAO precomputedConnectionsDAO;

    @Autowired
    private DbPrecomputedSimpleStopsDAO precomputedSimpleStopsDAO;

    @Autowired
    private DbPrecomputedStopsDAO precomputedStopsDAO;

    @Autowired
    private DbStationDAO stationDAO;

    @Autowired
    private DbSimpleStationDAO simpleStationDAO;

    @Autowired
    private DbConnectionDAO connectionDAO;

    private int numberOfSwitches;

    private List<SimpleParentStructure> queue;

    private long endTime;

    public HashSet<List<dbSimpleStation>> getAllRoutes() {
        return allRoutes;
    }

    public HashSet<List<dbSimpleStation>> allRoutes;

    private List<dbSimpleStation> stationOnConnections;




    public List<List<dbConnectedStation>> directConnectionSearch(Integer start, Integer finish)
    {
        try {

            int startStationId = start;
            int finishStationId = finish;
            if(start.equals(finish)){
                return new LinkedList<>();
            }

            stationRepository = stationDAO.findAll();
            for (dbStation temp_station : stationRepository){
                for ( dbSimpleStation simpleStation :temp_station.getSimpleStationConnections()){
                    simpleStation.visited=false;
                }

            }
            List<dbConnection> connections = connectionDAO.findAll();
            list = simpleStationDAO.findAll();

            startStation = getStationByID(start);
            finishStation = getStationByID(finish);
            // List<ArrivalHour> list2 = SQLite.select().from(ArrivalHour.class).queryList();
            // List<dbConnectionToHour> list3 = SQLite.select().from(dbConnectionToHour.class).queryList();
            // List<dbConnection> list4 = SQLite.select().from(dbConnection.class).queryList();
            //Toast toast = Toast.makeText(context, "Json error" + list1.size(), Toast.LENGTH_LONG);
            //toast.show();

            // System.out.printf("test");

        allRoutes = new HashSet<>();

        stationOnConnections = new LinkedList<>();

        queue = new LinkedList<>();

        dbStation station =getStationByID(startStation.getId());

        List<dbSimpleStation> connectedStations =
                new LinkedList<>(station.getSimpleStationConnections());

        HashSet<Integer> paths = new HashSet<>();
        queue = new ArrayList<>();
        allPaths = new LinkedList<>();
        stop = false;

        boolean no_paths_left = false;
        currentPath = new Stack<>();
        visited= new HashMap<>();
        for (dbStation  station1: stationRepository){
            visited.put(station1.getId(),false);
        }

        endTime = System.currentTimeMillis() + 10000;
        findPaths(connectedStations,connectedStations.get(0).motherStationID,finishStationId);

//        while (!no_paths_left) {
//
//            loop(connectedStations, null, null, 0, true, paths,no_paths_left);
//
//            for (dbStation temp_station : stationRepository){
//                for ( dbSimpleStation simpleStation :temp_station.getSimpleStationConnections()){
//                    simpleStation.visited=false;
//                }
//
//            }
//        }

        //   loop(connectedStations, null, null,true);
        allRoutes = new HashSet<>();
        processIntsToSimpleStation();

      //  retriveDataFromSearch();

        System.out.print(allRoutes.size());
        System.out.print("connection");
        List<PartStructure> connctionsList = findConnectionsByPath();
        if (connctionsList.size()>0){
            for (PartStructure structure : connctionsList) {
                int part_id = createPart();
                createSimpleStops(part_id ,structure.getStationList());
                createPrecomputedConnections(part_id, structure.getConnectionsList());
            }
        }
//        else if (allRoutes.size()>0){
//            int part_id = createPart();
//            createSimpleStops(part_id);
//        }
      // System.out.printf("etst")
            List<List<dbConnectedStation>> tempList = new LinkedList<>();
            for (PartStructure structure : connctionsList){
                tempList.addAll(structure.getConnectionsList());
            }

        return tempList;

        //return null;
        } catch (Exception ex) {
            System.out.println();
        }
        return null;
    }

    private Stack<Integer> currentPath;

    private HashMap<Integer,Boolean> visited;

    private List<List<Integer>> allPaths;

    private boolean stop;

    private void processIntsToSimpleStation(){
        List<dbSimpleStation> allStation = simpleStationDAO.findAll();
        for (List<Integer> path : allPaths){
            int i=0;
            List<dbSimpleStation> mySimpleList = new LinkedList<>();
            for (Integer stop : path){
                i++;
                final int temp = i;
                 if (i==path.size()) {
                   List<dbSimpleStation> result = allStation.stream().filter(p -> p.getMotherStationID() == stop)
                           .collect(Collectors.toList());
                   mySimpleList.add(result.get(0));
                 }
                 else {
                     List<dbSimpleStation> result = allStation.stream().filter(p -> p.getMotherStationID()==stop &&
                     p.getConnectedStationID() == path.get(temp)).collect(Collectors.toList());
                     mySimpleList.add(result.get(0));
                 }
            }
            allRoutes.add(mySimpleList);
        }
    }

    private void findPaths(List<dbSimpleStation> connedtedStation,Integer currentVertex, Integer destinationVertex){
        if (!stop) {
            currentPath.push(currentVertex);
            visited.put(currentVertex, true);
            if (System.currentTimeMillis() > endTime){
                stop=true;
            }
            if (currentVertex.equals(destinationVertex) && currentPath.size() != 1) {
              //  System.out.println("found path");
                allPaths.add(new LinkedList<Integer>(currentPath));
                endTime = System.currentTimeMillis() + 10000;
                if (allPaths.size() > 50) {
                    stop=true;
                }
            } else {


                for (dbSimpleStation currntStation : connedtedStation) {
                    try {
                        if (!visited.get(currntStation.connectedStationID)) {
                            List<dbSimpleStation> stationList =
                                    new LinkedList<>(getStationByID(currntStation.getConnectedStationID()).getSimpleStationConnections());
                            findPaths(stationList, currntStation.getConnectedStationID(), destinationVertex);
                        }
                    } catch (Exception ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }

            currentPath.pop();
            visited.put(currentVertex, false);
        }
        else
            return;



    }

    private void loop(List<dbSimpleStation> connectedStations, dbSimpleStation previousStation,
                       SimpleParentStructure structure,int current_level, boolean first_flag, HashSet<Integer> visited,boolean no_paths_left){

        current_level++;
        if (current_level>50){
            return;
        }
        int number_of_iter = connectedStations.size();
       // int previousNumber = numer_of_switches;
        try {
            SimpleParentStructure previousStructer = structure;
            for (int i = 0; i < number_of_iter; i++) {
             //   numer_of_switches = previousNumber;

                if (first_flag) {
                    previousStructer = null;
                    previousStation = null;
                    List<dbSimpleStation> list1 =
                           new LinkedList<>(getStationByID(connectedStations.get(i).connectedStationID).getSimpleStationConnections());
                    if (list1.size() > 0) { 
                        SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));

                        visited.add(connectedStations.get(i).getMotherStationID());
                        connectedStations.get(i).visited = true;
                        loop(list1, previousStation,
                                 p,current_level, false,visited,no_paths_left);
                    }
                }
                else {

                   if (connectedStations.get(i).motherStationID == finishStation.getId()) {
                        connectedStations.get(i).visited = true;
                        //stationOnConnections.add(connectedStations.get(i));

                        SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
                        p.setFinalStation(true);
                        p.setStructure(previousStructer);
                        queue.add(p);
                        break;

                    } else if (!visited.contains(connectedStations.get(i).getConnectedStationID())){
                        List<dbSimpleStation> list1 =
                               new LinkedList<>(getStationByID(connectedStations.get(i).connectedStationID).getSimpleStationConnections());

                            visited.add(connectedStations.get(i).getConnectedStationID());

                        if (list1.size() > 0) {

                            SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));

                            p.setStructure(previousStructer);

                            connectedStations.get(i).visited = true;

                            loop(list1, connectedStations.get(i),
                                   p,current_level, false,visited,no_paths_left);

                        }

                        connectedStations.get(i).visited = true;
                    }
//                   else {
//                       continue;
//                   }


                    connectedStations.get(i).visited = true;
                }

            }
        }
        catch (Exception ex){

           // System.out.printf("etst");
        }
    }

    private List<PartStructure> findConnectionsByPath(){
        ConnectionSearch search = new ConnectionSearch(stationDAO.findAll());

        for (List<dbSimpleStation> route : allRoutes){

            search.findConnections(route);
        }
        if (search.getAllRoutes().size()>0) {
            System.out.println();
        }
        return search.getPartStructures();
      //  return search.getAllRoutes();

    }

    private int createPart(){
        dbPrecomputedPart part = new dbPrecomputedPart();
        precomputedPartsDAO.save(part);
        return part.getId();
    }


    private void createSimpleStops(int part_id, List<dbSimpleStation> simpleStations){

            List<dbPrecomputedSimpleStop> simpleStops = new LinkedList<>();
            for (dbSimpleStation station : simpleStations){
                dbPrecomputedSimpleStop precomputedSimpleStop = new dbPrecomputedSimpleStop();
                precomputedSimpleStop.setPartID(part_id);
                precomputedSimpleStop.setStation_ID(station.motherStationID);
                precomputedSimpleStopsDAO.save(precomputedSimpleStop);
            }

        }


    private void createPrecomputedConnections(int part_id, List<List<dbConnectedStation>> connectionsList){

        for (List<dbConnectedStation> connectedStations : connectionsList){
            List<dbPrecomputedStop> precomputedStops = new LinkedList<>();
            dbPrecomputedConnection precomputedConnection = new dbPrecomputedConnection();
            precomputedConnection.setPart(part_id);
            precomputedConnectionsDAO.save(precomputedConnection);

            for (dbConnectedStation connectedStation : connectedStations){
                dbPrecomputedStop precomputedStop = new dbPrecomputedStop();
                precomputedStop.setConnectedStationID(connectedStation.getId());
                precomputedStop.setDbPrecomputedConnectionID(precomputedConnection.getId());
                precomputedStopsDAO.save(precomputedStop);
            }

        }

    }

    private dbStation getStationByID(int id){
        dbStation retStatio = new dbStation();
        for (dbStation station :stationRepository){
            //System.out.printf("test");
            if (station.getId() == id){
               // System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }


    private void retriveDataFromSearch(){
        for (SimpleParentStructure structure : queue){
            if (structure.isFinalStation()){
                retraceRoute(structure);
            }
        }

        //   mylist = new HashSet<>(allRoutes);
    }

    private void retraceRoute(SimpleParentStructure p){
        List<dbSimpleStation> route = new ArrayList<>();
        do {
            route.add(p.getStation());

            p = p.getStructure();
        }  while (p.getStructure()!=null);
        route.add(p.getStation());
        allRoutes.add(reversList(route));


       // System.out.printf("etst");
    }

    private List<dbSimpleStation> reversList(List<dbSimpleStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }

}
