package com.pathfinder.search;

import com.pathfinder.model.*;
import com.pathfinder.model.precomputedData.*;
import com.pathfinder.RailwayModel.structure.*;

import com.pathfinder.model.DAO.*;
import com.pathfinder.model.precomputedData.dbPrecomputedPart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Dominik on 18.04.2017.
 */

@Component("simpleSearch")
@Transactional
public class SimpleSearch {

    private dbStation startStation;

    private dbStation finishStation;

    private List<dbStation> stationRepository;

    private List<dbSimpleStation> list;

    @Autowired
    private DbStationDAO stationDAO;

    @Autowired
    private DbSimpleStationDAO simpleStationDAO;

    @Autowired
    private DbPrecomputedPartsDAO precomputedPartsDAO;

    @Autowired
    private DbPrecomputedConnectionsDAO precomputedConnectionsDAO;

    @Autowired
    private DbPrecomputedSimpleStopsDAO precomputedSimpleStopsDAO;

    @Autowired
    private DbPrecomputedStopsDAO precomputedStopsDAO;

    @Autowired
    private DbConnectionDAO connectionDAO;

    private int numberOfSwitches;

    private List<SimpleParentStructure> queue;

    private List<dbSimpleStation> stationOnConnections;



    public HashSet<List<dbSimpleStation>> getAllRoutes() {
        return allRoutes;
    }

    public HashSet<List<dbSimpleStation>> allRoutes;

    public List<List<dbConnectedStation>> directConnectionSearch(Integer start, Integer finish)
    {
        try {

            int startStationId = start;
            int finishStationId = finish;


            stationRepository = stationDAO.findAll();
            List<dbConnection> connections = connectionDAO.findAll();
            list = simpleStationDAO.findAll();

            startStation = getStationByID(start);
            finishStation = getStationByID(finish);
            // List<ArrivalHour> list2 = SQLite.select().from(ArrivalHour.class).queryList();
            // List<dbConnectionToHour> list3 = SQLite.select().from(dbConnectionToHour.class).queryList();
            // List<dbConnection> list4 = SQLite.select().from(dbConnection.class).queryList();
            //Toast toast = Toast.makeText(context, "Json error" + list1.size(), Toast.LENGTH_LONG);
            //toast.show();

            // System.out.printf("test");
        } catch (Exception ex) {

        }
        allRoutes = new HashSet<>();

        stationOnConnections = new LinkedList<>();

        queue = new LinkedList<>();

        dbStation station =getStationByID(startStation.getId());

        List<dbSimpleStation> connectedStations =
                new LinkedList<>(station.getSimpleStationConnections());


        queue = new ArrayList<>();
        bfs();


        //   loop(connectedStations, null, null,true);

        retriveDataFromSearch();

        System.out.print(allRoutes.size());
        System.out.print("connection");
        List<List<dbConnectedStation>> connctionsList = findConnectionsByPath();
        if (connctionsList.size()>0){
            int part_id = createPart();
            createSimpleStops(part_id);
            createPrecomputedConnections(part_id, connctionsList);
        }
        else if (allRoutes.size()>0){
            int part_id = createPart();
            createSimpleStops(part_id);
        }
        System.out.printf("etst");



        return connctionsList;

        //return null;
    }

    private int createPart(){
        dbPrecomputedPart part = new dbPrecomputedPart();
        precomputedPartsDAO.save(part);
        return part.getId();
    }


    private void createSimpleStops(int part_id){
        for (List<dbSimpleStation> simpleStations : allRoutes){
            List<dbPrecomputedSimpleStop> simpleStops = new LinkedList<>();
            for (dbSimpleStation station : simpleStations){
                dbPrecomputedSimpleStop precomputedSimpleStop = new dbPrecomputedSimpleStop();
                precomputedSimpleStop.setPartID(part_id);
                precomputedSimpleStop.setStation_ID(station.motherStationID);
                precomputedSimpleStopsDAO.save(precomputedSimpleStop);
            }

        }
    }

    private void createPrecomputedConnections(int part_id, List<List<dbConnectedStation>> connectionsList){

        for (List<dbConnectedStation> connectedStations : connectionsList){
            List<dbPrecomputedStop> precomputedStops = new LinkedList<>();
            dbPrecomputedConnection precomputedConnection = new dbPrecomputedConnection();
            precomputedConnection.setPart(part_id);
            precomputedConnectionsDAO.save(precomputedConnection);

            for (dbConnectedStation connectedStation : connectedStations){
                dbPrecomputedStop precomputedStop = new dbPrecomputedStop();
                precomputedStop.setConnectedStationID(connectedStation.getId());
                precomputedStop.setDbPrecomputedConnectionID(precomputedConnection.getId());
                precomputedStopsDAO.save(precomputedStop);
            }

        }

    }

    private List<List<dbConnectedStation>> findConnectionsByPath(){
        ConnectionSearch search = new ConnectionSearch(stationDAO.findAll());
        for (List<dbSimpleStation> route : allRoutes){

            search.findConnections(route);
        }
        if (search.getAllRoutes().size()>0) {
            System.out.println();
        }
        return search.getAllRoutes();

    }

    private void bfs(){
        try {
            List<dbSimpleStation> connectedStations =
                  new LinkedList<>(getStationByID(startStation.getId()).getSimpleStationConnections());
            SimpleParentStructure p;
            for (dbSimpleStation station : connectedStations) {
                p = new SimpleParentStructure(null, station);
                p.setStructure(null);
                station.structure = p;
            }
            queue = new LinkedList<>();
            LinkedList<dbSimpleStation> nextToVisit = new LinkedList<>();
            HashSet<Integer> visited = new HashSet<>();
            nextToVisit.addAll(connectedStations);
            dbSimpleStation currentStation;
            SimpleParentStructure previousStructer = new SimpleParentStructure(null, null);
            int number_of_switches = 0;
            dbConnectedStation previousStation = new dbConnectedStation();

            while (!nextToVisit.isEmpty()) {

                currentStation = nextToVisit.remove();


                if (currentStation.getMotherStationID() == finishStation.getId()) {
                    //System.out.printf("test");
//                    p = new SimpleParentStructure(currentStation.structure.getStation(), currentStation);
//                    p.setFinalStation(true);
//                    p.setStructure(currentStation.structure);
                    currentStation.structure.setFinalStation(true);
                    queue.add(currentStation.structure);
                   // allRoutes = new HashSet<>();
                    //retriveDataFromSearch();
                    return;
                }


                if (visited.contains(currentStation.getId())) {
                    continue;
                }

                visited.add(currentStation.getId());

                //  previousStation = currentStation;
                List<dbSimpleStation> myConStation =
                       new LinkedList<>(getStationByID(currentStation.getConnectedStationID()).getSimpleStationConnections());

                for (dbSimpleStation station : myConStation) {
                    if (visited.contains(station.getId())) {
                        continue;
                    } else {
                        if (currentStation.structure!=null) {
                            p = new SimpleParentStructure(currentStation, station);

                            p.setStructure(currentStation.structure);
                            station.structure = p;
                        }

                        nextToVisit.add(station);
                    }
                }


            }
        }
        catch (Exception ex){
            System.out.printf("etst");
        }

        allRoutes = new HashSet<>();
        retriveDataFromSearch();

        System.out.printf("etst");




    }

    private void loop2(List<dbSimpleStation> connectedStations,List<dbSimpleStation> route, dbSimpleStation previousStation,
                       SimpleParentStructure structure,int current_level,int number_of_switches,boolean first_flag){

//
//        int number_of_iter = connectedStations.size();
//       // int previousNumber = numer_of_switches;
//        try {
//            SimpleParentStructure previousStructer = structure;
//            for (int i = 0; i < number_of_iter; i++) {
//             //   numer_of_switches = previousNumber;
//                if (first_flag) {
//                    previousStructer = null;
//                    previousStation = null;
//                    List<dbSimpleStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getSimpleStationConnections();
//                    if (list1.size() > 0) {
//                        SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
//
//
//                        connectedStations.get(i).visited = true;
//                        loop(list1, previousStation,
//                                 p, false);
//
//                    }
//
//                }
//
//                else  {
//
//
//                   if (connectedStations.get(i).motherStationID == finishStation.id && !(connectedStations.get(i).visited)) {
//                        connectedStations.get(i).visited = true;
//                        stationOnConnections.add(connectedStations.get(i));
//
//                        SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
//                        p.setFinalStation(true);
//                        p.setStructure(previousStructer);
//                        queue.add(p);
//                        break;
//
//                    } else {
//                        List<dbSimpleStation> list1 = getStationByID(connectedStations.get(i).connectedStationID).getSimpleStationConnections();
//                        if (list1.size() > 0) {
//                            SimpleParentStructure p = new SimpleParentStructure(previousStation, connectedStations.get(i));
//
//
//                            p.setStructure(previousStructer);
//
//                            connectedStations.get(i).visited = true;
//                            loop(list1, connectedStations.get(i),
//                                   p, false);
//
//                        }
//                        connectedStations.get(i).visited = true;
//                    }
//                    connectedStations.get(i).visited = true;
//                }
//
//            }
//        }
//        catch (Exception ex){
//            Toast toast = Toast.makeText(context,"Json error"+ex.getMessage(),Toast.LENGTH_LONG);
//            toast.show();
//            System.out.printf("etst");
//        }
    }




    private dbStation getStationByID(int id){
        dbStation retStatio = new dbStation();
        for (dbStation station : stationRepository){
           // System.out.printf("test");
            if (station.getId() == id){
             //   System.out.printf("test");
                retStatio = station;
                break;
            }
        }
        return retStatio;
    }

    private void retriveDataFromSearch(){
        for (SimpleParentStructure structure : queue){
            if (structure.isFinalStation()){
                retraceRoute(structure);
            }
        }

        //   mylist = new HashSet<>(allRoutes);
    }

    private void retraceRoute(SimpleParentStructure p){
        List<dbSimpleStation> route = new ArrayList<>();
        do {
            route.add(p.getStation());

            p = p.getStructure();
        }  while (p.getStructure()!=null);
        route.add(p.getStation());
        allRoutes.add(reversList(route));


        System.out.printf("etst");
    }

    private List<dbSimpleStation> reversList(List<dbSimpleStation> connectedStations){
        Collections.reverse(connectedStations);
        return  connectedStations;
    }

//
//    private dbStation getStationIdByName(String stationName){
//        return  SQLite.select().from(dbStation.class).where(dbStation_Table.stationName.eq(stationName)).queryList().get(0);
//    }
}
