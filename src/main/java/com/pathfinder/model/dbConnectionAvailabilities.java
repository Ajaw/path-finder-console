package com.pathfinder.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
public class dbConnectionAvailabilities {
    private int id;
    private Timestamp beggingDate;
    private Timestamp endDate;
    private Integer connectionId;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "beggingDate")
    public Timestamp getBeggingDate() {
        return beggingDate;
    }

    public void setBeggingDate(Timestamp beggingDate) {
        this.beggingDate = beggingDate;
    }

    @Basic
    @Column(name = "endDate")
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "connection_ID")
    public Integer getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(Integer connectionId) {
        this.connectionId = connectionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectionAvailabilities that = (dbConnectionAvailabilities) o;

        if (id != that.id) return false;
        if (beggingDate != null ? !beggingDate.equals(that.beggingDate) : that.beggingDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (connectionId != null ? !connectionId.equals(that.connectionId) : that.connectionId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (beggingDate != null ? beggingDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (connectionId != null ? connectionId.hashCode() : 0);
        return result;
    }
}
