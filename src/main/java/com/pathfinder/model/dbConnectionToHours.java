package com.pathfinder.model;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
public class dbConnectionToHours {
    private int id;
    private int connectionId;
    private boolean onWeekendConnection;
    private Collection<ArrivalHour> conHoursList;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "connectionId")
    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    @Basic
    @Column(name = "onWeekendConnection")
    public boolean getOnWeekendConnection() {
        return onWeekendConnection;
    }

    public void setOnWeekendConnection(boolean onWeekendConnection) {
        this.onWeekendConnection = onWeekendConnection;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectionToHours that = (dbConnectionToHours) o;

        if (id != that.id) return false;
        if (connectionId != that.connectionId) return false;
        if (onWeekendConnection != that.onWeekendConnection) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + connectionId;

        return result;
    }

    @OneToMany(mappedBy = "connectionToHourId")
    public Collection<ArrivalHour> getConHoursList() {
        return conHoursList;
    }

    public void setConHoursList(Collection<ArrivalHour> conHoursList) {
        this.conHoursList = conHoursList;
    }
}
