package com.pathfinder.model;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
@Table(name = "dbStations")
public class dbStation {
    private int id;
    private String stationName;
    private Collection<dbSimpleStation> simpleStationConnections;
    private Collection<dbConnectedStation> directStationConnection;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "stationName")
    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbStation that = (dbStation) o;

        if (id != that.id) return false;
        if (stationName != null ? !stationName.equals(that.stationName) : that.stationName != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (stationName != null ? stationName.hashCode() : 0);
        return result;
    }


    @OneToMany(mappedBy = "motherStationID")
    public Collection<dbSimpleStation> getSimpleStationConnections() {
        return simpleStationConnections;
    }

    public void setSimpleStationConnections(Collection<dbSimpleStation> simpleStationConnections) {
        this.simpleStationConnections = simpleStationConnections;
    }


    @OneToMany(mappedBy = "motherStationID")
    public Collection<dbConnectedStation> getDirectStationConnection() {
        return directStationConnection;
    }


    public void setDirectStationConnection(Collection<dbConnectedStation> directStationConnection) {
        this.directStationConnection = directStationConnection;
    }

}
