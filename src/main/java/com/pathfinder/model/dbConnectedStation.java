package com.pathfinder.model;

import com.pathfinder.RailwayModel.structure.ParentStructure;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
@Table(name = "dbConnectedStations")
public class dbConnectedStation{
    public int id;
    public int motherStationID;
    public int connectedStationID;
    public int previousStationID;
    public int connectionID;
    public boolean finalStation;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "motherStationID")
    public int getMotherStationID() {
        return motherStationID;
    }

    public void setMotherStationID(int motherStationID) {
        this.motherStationID = motherStationID;
    }

    @Basic
    @Column(name = "connectedStationID")
    public int getConnectedStationID() {
        return connectedStationID;
    }

    public void setConnectedStationID(int connectedStationId) {
        this.connectedStationID = connectedStationId;
    }

    @Basic
    @Column(name = "previousStationID")
    public int getPreviousStationID() {
        return previousStationID;
    }

    public void setPreviousStationID(int previousStationID) {
        this.previousStationID = previousStationID;
    }

    @Basic
    @Column(name = "connectionID")
    public int getConnectionID() {
        return connectionID;
    }

    public void setConnectionID(int connectionID) {
        this.connectionID = connectionID;
    }

    @Basic
    @Column(name = "finalStation")
    @Type(type = "org.hibernate.type.NumericBooleanType")
    public boolean getFinalStation() {
        return finalStation;
    }

    public void setFinalStation(boolean finalStation) {
        this.finalStation = finalStation;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnectedStation that = (dbConnectedStation) o;

        if (id != that.id) return false;
        if (motherStationID != that.motherStationID) return false;
        if (connectedStationID != that.connectedStationID) return false;
        if (previousStationID != that.previousStationID) return false;
        if (connectionID != that.connectionID) return false;
        if (finalStation != that.finalStation) return false;

        return true;
    }


    @Transient
    public  boolean visited;
    @Transient
    public int number_of_switches;
    @Transient
    public ParentStructure structure;
}
