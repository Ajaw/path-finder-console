package com.pathfinder.model;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
@Table(name = "ArrivalHours")
public class ArrivalHour {
    private int id;
    private int connectionToHourId;
    private int stationOnConnectionId;
    private Timestamp arrivalTime;
    private Timestamp departureTime;
    private String platform;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "newStationID")
    public int getStationOnConnectionId() {
        return stationOnConnectionId;
    }

    public void setStationOnConnectionId(int stationOnConnectionId) {
        this.stationOnConnectionId = stationOnConnectionId;
    }

    @Basic
    @Column(name = "connectionToHourId")
    public int getConnectionToHourId() {
        return connectionToHourId;
    }

    public void setConnectionToHourId(int connectionToHourId) {
        this.connectionToHourId = connectionToHourId;
    }

    @Basic
    @Column(name = "arrivalTime")
    public Timestamp getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(Timestamp arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Basic
    @Column(name = "departureTime")
    public Timestamp getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(Timestamp departureTime) {
        this.departureTime = departureTime;
    }

    @Basic
    @Column(name = "platform")
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ArrivalHour that = (ArrivalHour) o;

        if (id != that.id) return false;
        if (connectionToHourId != that.connectionToHourId) return false;
        if (arrivalTime != null ? !arrivalTime.equals(that.arrivalTime) : that.arrivalTime != null) return false;
        if (departureTime != null ? !departureTime.equals(that.departureTime) : that.departureTime != null)
            return false;
        if (platform != null ? !platform.equals(that.platform) : that.platform != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + connectionToHourId;
        result = 31 * result + (arrivalTime != null ? arrivalTime.hashCode() : 0);
        result = 31 * result + (departureTime != null ? departureTime.hashCode() : 0);
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        return result;
    }
}
