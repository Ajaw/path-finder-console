package com.pathfinder.model.precomputedData;

import java.util.List;
import com.pathfinder.model.*;
/**
 * Created by Dominik on 17.05.2017.
 */
public class PartStructure {

    public List<dbSimpleStation> getStationList() {
        return stationList;
    }

    public void setStationList(List<dbSimpleStation> stationList) {
        this.stationList = stationList;
    }

    public List<List<dbConnectedStation>> getConnectionsList() {
        return connectionsList;
    }

    public void setConnectionsList(List<List<dbConnectedStation>> connectionsList) {
        this.connectionsList = connectionsList;
    }

    private List<dbSimpleStation> stationList;

    private List<List<dbConnectedStation>> connectionsList;

}
