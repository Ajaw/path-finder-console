package com.pathfinder.model.precomputedData;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 09.05.2017.
 */
@Entity
@Table(name = "dbPrecomputedParts")
public class dbPrecomputedPart {

    private int id;
    private Collection<dbPrecomputedConnection> connections;
    private Collection<dbPrecomputedSimpleStop> stopsOnStation;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbPrecomputedPart that = (dbPrecomputedPart) o;

        if (id != that.id) return false;

        return true;
    }







    @Override
    public int hashCode() {
        return id;
    }
}
