package com.pathfinder.model.precomputedData;

import javax.persistence.*;

/**
 * Created by Dominik on 09.05.2017.
 */
@Entity
@Table(name = "dbPrecomputedConnections")
public class dbPrecomputedConnection {

    private int id;
    private int Part;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    @Column(name = "precomputedPartID")
    public int getPart() {
        return Part;
    }

    public void setPart(int part) {
        Part = part;
    }


}
