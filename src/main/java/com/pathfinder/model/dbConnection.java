package com.pathfinder.model;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
@Table(name = "dbConnections")
public class dbConnection {
    private int id;
    private Collection<dbConnectionToHours> hoursList;
    private Collection<dbStationOnConnection> stationsList;

    @Id
    @Column(name = "ID")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbConnection that = (dbConnection) o;

        if (id != that.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @OneToMany(targetEntity = dbConnectionToHours.class ,mappedBy = "connectionId")
    public Collection<dbConnectionToHours> getHoursList() {
        return hoursList;
    }

    public void setHoursList(Collection<dbConnectionToHours> hoursList) {
        this.hoursList = hoursList;
    }

    @OneToMany(targetEntity = dbStationOnConnection.class ,mappedBy = "connectionId")
    public Collection<dbStationOnConnection> getStationsList() {
        return stationsList;
    }

    public void setStationsList(Collection<dbStationOnConnection> stationsList) {
        this.stationsList = stationsList;
    }
}
