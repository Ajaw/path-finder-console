package com.pathfinder.model;

import com.pathfinder.RailwayModel.structure.SimpleParentStructure;

import javax.persistence.*;

/**
 * Created by Dominik on 07.05.2017.
 */
@Entity
@Table(name = "dbSimpleStations")
public class dbSimpleStation {
    public int id;
    public int connectedStationID;
    public int motherStationID;
    public int previousStationID;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "connectedStationID")
    public int getConnectedStationID() {
        return connectedStationID;
    }

    public void setConnectedStationID(int connectedStationID) {
        this.connectedStationID = connectedStationID;
    }

    @Basic
    @Column(name = "motherStationID")
    public int getMotherStationID() {
        return motherStationID;
    }

    public void setMotherStationID(int motherStationID) {
        this.motherStationID = motherStationID;
    }

    @Basic
    @Column(name = "previousStationID")
    public int getPreviousStationID() {
        return previousStationID;
    }

    public void setPreviousStationID(int previousStationID) {
        this.previousStationID = previousStationID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        dbSimpleStation that = (dbSimpleStation) o;

        if (id != that.id) return false;
        if (connectedStationID != that.connectedStationID) return false;
        if (motherStationID != that.motherStationID) return false;
        if (previousStationID != that.previousStationID) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + connectedStationID;
        result = 31 * result + motherStationID;
        result = 31 * result + previousStationID;
        return result;
    }

    public  boolean visited;

    public SimpleParentStructure structure;
}
