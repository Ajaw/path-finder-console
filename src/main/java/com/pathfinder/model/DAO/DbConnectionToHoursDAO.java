package com.pathfinder.model.DAO;


import com.pathfinder.model.dbConnectionToHours;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 08.03.2017.
 */
@Repository
public interface DbConnectionToHoursDAO extends CrudRepository<dbConnectionToHours,String> {

    public List<dbConnectionToHours> findAll();

}
