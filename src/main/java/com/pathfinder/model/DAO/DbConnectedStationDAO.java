package com.pathfinder.model.DAO;

import com.pathfinder.model.*;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 26.03.2017.
 */
@Repository
public interface DbConnectedStationDAO extends CrudRepository<dbConnectedStation,String> {

    public List<dbConnectedStation> findAll();



}
