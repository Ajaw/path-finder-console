package com.pathfinder.model.DAO;

import com.pathfinder.model.precomputedData.dbPrecomputedConnection;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
public interface DbPrecomputedConnectionsDAO  extends CrudRepository<dbPrecomputedConnection,String> {

    public List<dbPrecomputedConnection> findAll();


}
