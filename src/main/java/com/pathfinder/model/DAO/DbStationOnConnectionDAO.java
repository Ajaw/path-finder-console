package com.pathfinder.model.DAO;

import com.pathfinder.model.dbStationOnConnection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 01.04.2017.
 */
@Repository
public interface DbStationOnConnectionDAO extends CrudRepository<dbStationOnConnection,String> {

    public List<dbStationOnConnection> findAll();

}
