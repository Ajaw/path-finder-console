package com.pathfinder.model.DAO;

import com.pathfinder.model.dbConnection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 26.03.2017.
 */
@Repository
public interface DbConnectionDAO extends CrudRepository<dbConnection,String> {

    public List<dbConnection> findAll();

}
