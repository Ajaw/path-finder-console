package com.pathfinder.model.DAO;

import com.pathfinder.model.precomputedData.dbPrecomputedStop;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
public interface DbPrecomputedStopsDAO extends CrudRepository<dbPrecomputedStop,String> {

    public List<dbPrecomputedStop> findAll();

}
