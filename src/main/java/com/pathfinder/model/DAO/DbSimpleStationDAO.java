package com.pathfinder.model.DAO;


import com.pathfinder.model.*;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 08.03.2017.
 */
@Repository
public interface DbSimpleStationDAO extends CrudRepository<dbSimpleStation,String> {

    public List<dbSimpleStation> findAll();

    public List<dbSimpleStation> findByMotherStationIDAndConnectedStationID(int motherStation, int connectedStation);

    public List<dbSimpleStation> findByMotherStationID(int motherStaion);

}
