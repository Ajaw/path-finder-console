package com.pathfinder.model.DAO;

import com.pathfinder.model.precomputedData.dbPrecomputedSimpleStop;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
public interface DbPrecomputedSimpleStopsDAO extends CrudRepository<dbPrecomputedSimpleStop,String> {

    public List<dbPrecomputedSimpleStop> findAll();

}
