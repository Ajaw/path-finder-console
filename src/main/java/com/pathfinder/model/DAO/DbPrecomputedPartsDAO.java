package com.pathfinder.model.DAO;

import com.pathfinder.model.precomputedData.dbPrecomputedPart;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 09.05.2017.
 */
public interface DbPrecomputedPartsDAO  extends CrudRepository<dbPrecomputedPart,String> {

    public List<dbPrecomputedPart> findAll();

}
