package com.pathfinder.model.DAO;

import com.pathfinder.model.dbStation;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Dominik on 31.03.2017.
 */
@Repository
public interface DbStationDAO extends CrudRepository<dbStation,String> {

    public List<dbStation> findAll();

}
