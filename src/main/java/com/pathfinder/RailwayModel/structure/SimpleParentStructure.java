package com.pathfinder.RailwayModel.structure;

import com.pathfinder.model.*;
/**
 * Created by Dominik on 18.04.2017.
 */

public class SimpleParentStructure {

    public SimpleParentStructure(dbSimpleStation _parentStation, dbSimpleStation _station){
        parentStation = _parentStation;
        station = _station;
    }

    public dbSimpleStation getParentStation() {
        return parentStation;
    }

    public dbSimpleStation getStation() {
        return station;
    }

    public SimpleParentStructure getStructure() {
        return structure;
    }

    public dbSimpleStation parentStation;

    public dbSimpleStation station;

    public void setStructure(SimpleParentStructure structure) {
        this.structure = structure;
    }

    public void setFinalStation(boolean finalStation) {
        isFinalStation = finalStation;
    }

    public SimpleParentStructure structure;

    public boolean isFinalStation() {
        return isFinalStation;
    }

    public boolean isFinalStation;

}
