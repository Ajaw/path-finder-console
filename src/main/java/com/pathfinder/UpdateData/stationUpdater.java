package com.pathfinder.UpdateData;



import com.pathfinder.PathFinderConsoleApplication;
import com.pathfinder.model.*;

import com.pathfinder.model.DAO.*;
import com.pathfinder.search.DfsSearch;
import com.pathfinder.search.SimpleSearch;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


/**
 * Created by Dominik on 07.04.2017.
 */

@Component("stationUpdater")
@Transactional
public class stationUpdater {

    private List<dbConnection> connectionList;

    @Autowired
    private DbConnectionDAO connectionDAO;

    @Autowired
    private DbConnectedStationDAO connectedStationDAO;

    @Autowired
    private DbSimpleStationDAO simpleStationDAO;

    @Autowired
    private DbPrecomputedPartsDAO precomputedPartsDAO;

    @Autowired
    private DbPrecomputedConnectionsDAO precomputedConnectionsDAO;

    @Autowired
    private DbPrecomputedSimpleStopsDAO precomputedSimpleStopsDAO;

    @Autowired
    private DbPrecomputedStopsDAO precomputedStopsDAO;


//    @Resource(name="simpleSearch")
//    private SimpleSearch search;

    @Resource(name = "dfsSearch")
    private DfsSearch dfsSearch;



    public void updateStationsConnetion(){
        try {
            //List<List<dbConnectedStation>> tempRoute =   dfsSearch.directConnectionSearch(290,282);

            connectionList = connectionDAO.findAll();

            List<dbConnectedStation> connectedStations = new ArrayList<>();
            dbStationOnConnection nextStation;

            int counter = 1;
            for (dbConnection connection : connectionList) {
                List<dbStationOnConnection> stationOnConnections =
                       new LinkedList<>(connection.getStationsList());
                dbConnectedStation previousStation = new dbConnectedStation();
                int i = 0;
                while (stationOnConnections.size() > i) {

                    dbStationOnConnection currentStation = stationOnConnections.get(i);
                    dbConnectedStation connectedStation2 = new dbConnectedStation();
                    if (i != 0) {
                        connectedStation2.previousStationID = previousStation.id;
                    }
                    if (i < stationOnConnections.size()-1) {
                        nextStation = stationOnConnections.get(i+1);
                        connectedStation2.id =counter;
                        connectedStation2.motherStationID = currentStation.getStationId();
                        connectedStation2.connectedStationID = nextStation.getStationId();
                        connectedStation2.connectionID = currentStation.getConnectionId();
                        connectedStations.add(connectedStation2);
                        previousStation = connectedStation2;
                    }
                    else {
                        connectedStation2.id =counter;
                        connectedStation2.motherStationID = currentStation.getStationId();
                        connectedStation2.connectionID = currentStation.getConnectionId();
                        connectedStation2.finalStation = true;
                        connectedStations.add(connectedStation2);
                    }

                    i++;
                    counter++;
                }

            }
            System.out.println("ha");
           connectedStationDAO.save(connectedStations);

//            SQLite.delete(dbConnectedStation.class).executeUpdateDelete();
//            ModelAdapter<dbConnectedStation> modelAdapter = FlowManager.getModelAdapter(dbConnectedStation.class);
//
//            TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
//                    .insertBuilder(modelAdapter).addAll(connectedStations).build());

         //  makeSimpleStationConnections();

//            List<List<dbConnectedStation>> allRoutes= new LinkedList<>();
//            int currect_pos =1;
//            List<dbConnection> connetionsToLookup = new LinkedList<>();
//            for (dbConnection connection : connectionList) {
//                int postion = contatinsId(connection.getStationList(),419);
//                if (postion!=0){
//                    try {
//                        if (connection.getStationList().get(postion + 2).dbStationId == 318) {
//                            connetionsToLookup.add(connection);
//                        }
//                    }
//                    catch (Exception ex){
//                        System.out.printf("test");
//                    }
//                }
//            }
//
//
//            for (dbConnection connection : connetionsToLookup) {
//                PathFinder finder = new PathFinder();
//                if (connection.getStationList().size()>0) {
//                    finder.dfs(connection);
//                    allRoutes.addAll(finder.allRoutes);
//                }
//                else {
//                    System.out.printf("tes");
//                }
//                currect_pos++;
//            }
//            System.out.printf("tes");
//

//
//
//            SQLite.delete(PrecomputedPath.class).executeUpdateDelete();
//            SQLite.delete(PrecomputedStation.class).executeUpdateDelete();
//
//            counter =1;
//            int counter2=1;
//            ModelAdapter<PrecomputedPath> pathAdapater = FlowManager.getModelAdapter(PrecomputedPath.class);
//            ModelAdapter<PrecomputedStation> patStationhAdapater = FlowManager.getModelAdapter(PrecomputedStation.class);
//
//            for (List<dbConnectedStation> connectedStations1 : allRoutes){
//                PrecomputedPath path = new PrecomputedPath();
//                path.setStationsList(connectedStations1);
//                path.setId(counter);
//                List<PrecomputedStation> precomputedStations = new LinkedList<>();
//                for (dbConnectedStation connectedStation : connectedStations1){
//
//                    PrecomputedStation precomputedStation = new PrecomputedStation();
//                    precomputedStation.setConnectedStationID(connectedStation.id);
//                    precomputedStation.setPrecomputedPathID(counter);
//                    precomputedStation.setId(counter2);
//                    precomputedStations.add(precomputedStation);
//                            counter2++;
//                }
//                TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
//                        .insertBuilder(patStationhAdapater).addAll(precomputedStations).build());
//                TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
//                        .insertBuilder(pathAdapater).add(path).build());
//                counter++;
//            }

            System.out.printf("tes");

        }
        catch (Exception ex){
            System.out.printf("test");
        }

    }

    private  int contatinsId(List<dbStationOnConnection> stations, int id){
        int counter = 0;
        for (dbStationOnConnection station: stations){
            if (station.getStationId() == id){
                System.out.printf("test");
                return counter;
            }
            counter++;
        }
        return 0;
    }

    public void makeSimpleStationConnections(){
       // SQLite.delete(dbSimpleStation.class).executeUpdateDelete();
        connectionList = connectionDAO.findAll();
        simpleStationDAO.deleteAll();
        List<dbSimpleStation> toAdd = new LinkedList<>();
        int counter =1;
        for (dbConnection connection : connectionList) {
            try {


                List<dbStationOnConnection> stationOnConnections =
                        new LinkedList<>(connection.getStationsList());
                dbSimpleStation previousStation = new dbSimpleStation();
                dbStationOnConnection nextStation = new dbStationOnConnection();

                List<dbSimpleStation> connectedStations = new LinkedList<>();
                int i = 0;

                while (stationOnConnections.size() > i) {

                    dbStationOnConnection currentStation = stationOnConnections.get(i);
                    dbSimpleStation connectedStation2 = new dbSimpleStation();
                    if (i != 0) {
                        connectedStation2.previousStationID = previousStation.motherStationID;
                    }
                    if (i < stationOnConnections.size() - 1) {
                        nextStation = stationOnConnections.get(i + 1);
                        connectedStation2.id = counter;

                        connectedStation2.motherStationID = currentStation.getStationId();
                        connectedStation2.connectedStationID = nextStation.getStationId();
                        if (previousStation.motherStationID != 0 || nextStation.getStationId() != 0) {
                             if (!checkIfSimpleStationExists(currentStation.getStationId(), nextStation.getStationId(),toAdd)) {
                            connectedStations.add(connectedStation2);
                            if (counter==2209){
                                System.out.println("stop");
                            }
                            previousStation = connectedStation2;

                            System.out.println(counter);
                            counter++;
                             }
                        }
                    }
//                else {
//                    connectedStation2.id =counter;
//                    connectedStation2.motherStationID = currentStation.dbStationId;
//                    if (previousStation.motherStationID!=0) {
//                        connectedStations.add(connectedStation2);
//                        counter++;
//                    }
//                }

                    i++;

                }
                if (connectedStations.size()>0) {
                    toAdd.addAll(connectedStations);
                }
            }
            catch (Exception ex){
                System.out.println(ex.getMessage());
            }

//            ModelAdapter<dbSimpleStation> modelAdapter = FlowManager.getModelAdapter(dbSimpleStation.class);
//            TrainFinderApplication.instance.database.executeTransaction(FastStoreModelTransaction
//                    .insertBuilder(modelAdapter).addAll(connectedStations).build());

        }

        HashSet<Integer> values = new HashSet<>();
        for (dbSimpleStation station  : toAdd){
            values.add(station.getId());
        }

        simpleStationDAO.save(toAdd);
    }

    public void getAllConnections(){
                  List<dbConnection> connectionsList = connectionDAO.findAll();
                                precomputedConnectionsDAO.deleteAll();
                                precomputedPartsDAO.deleteAll();
                                precomputedSimpleStopsDAO.deleteAll();
                                precomputedStopsDAO.deleteAll();
                                int i =0;
                                HashSet<Integer> beginVisited = new HashSet<>();
                                for (dbConnection connection : connectionsList){
                                    List<List<dbConnectedStation>> routes = new LinkedList<>();
                                    if (connection.getStationsList().size()>0) {
                                        dbStationOnConnection start = new LinkedList<>(connection.getStationsList()).getFirst();
                                        if (!beginVisited.contains(start.getStationId())) {
                                            HashSet<Integer> visitedStations = new HashSet<>();
                                            HashSet<List<dbSimpleStation>> allRoutes = new HashSet<>();
                                            for (dbConnection endConnection : connectionsList) {
                                                if (endConnection.getStationsList().size() > 0) {
                                                    try {
                                                        if (!visitedStations.contains(
                                                                new LinkedList<>(endConnection.getStationsList()).getLast().getStationId())) {
                                                            dbStationOnConnection end = new LinkedList<>(endConnection.getStationsList()).getLast();

                                                            // List<List<dbConnectedStation>> routeNow = search.directConnectionSearch(start.getStationId(), end.getStationId());
                                                            List<List<dbConnectedStation>> routeNow = dfsSearch.directConnectionSearch(start.getStationId(),end.getStationId());
                                                            System.out.println();
                                                            if (routeNow.size() > 0) {
                                                                routes.addAll(routeNow);
                                                            }
                                                            visitedStations.add(end.getStationId());
                                                            if (dfsSearch.getAllRoutes().size() > 0) {
                                                                allRoutes.addAll(dfsSearch.getAllRoutes());
                                                            }
                                                        } else {
                                                            System.out.println("visited");
                                                        }
                                                    }
                            catch (Exception ex){
                              //  System.out.println(ex.getMessage());
                            }
                        }
                    }
                  //  i++;
//                    if (i>3){
//                        break;
//                    }
                    beginVisited.add(start.getStationId());
                    System.out.println(routes.size());
                    System.out.println(allRoutes.size());
                    System.out.println("test");
                }
            }
        }
        System.out.println(connectionsList.size());
    }

    private boolean checkIfSimpleStationExists(int motherStation, int connectedStation,List<dbSimpleStation> toAdd){
        try {
            for (dbSimpleStation station : toAdd) {
                if (station.getMotherStationID() == motherStation && connectedStation == station.getConnectedStationID()) {
                    System.out.println("dasdas");
                    return true;
                }
            }
            return false;
//            if (station.size() > 0) {
//                System.out.println("dasdas");
//                return true;
//            } else
//                System.out.println("dasdas");
//                return false;
//        }
        }
        catch (Exception ex){
            return  false;
        }
    }

}
